import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, AbstractControl, Validators} from '@angular/forms';
import { AlertController } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { ToastController } from 'ionic-angular';

import { AuthService  } from '../../providers/getdata/authService';
import { HomePage } from '../home/home';

import { GetdataProvider } from '../../providers/getdata/getdata';

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  users = [];

  testCheckboxOpen: boolean;
  testCheckboxResult;
  music: string;
  mTongue :any;
  gender :any = "male";
  religion :any;
  fromAge:any;
  toAge:any;
  messages : string = '';
  responseData : any;
  isAndroid: boolean = false;
  masterR : any[];
  masterM : any[];

  Mariage: { title: string, subTitle: string };

  constructor(public navCtrl: NavController,private authService:AuthService,private getData:GetdataProvider,public toastCtrl: ToastController, public navParams: NavParams,public formbuilder: FormBuilder,public alertCtrl: AlertController,platform: Platform) {

    this.isAndroid = platform.is('android');
    this.mTongue = navParams.get('mTongue');
    this.gender = navParams.get('gender');
    this.religion = navParams.get('religion');
    this.fromAge = navParams.get('fromAge');
    this.toAge = navParams.get('toAge');



  }
    userSearch = {"gender": this.gender,"mTongue": this.mTongue,"religion":this.religion,"fromAge":this.fromAge,"toAge":this.toAge};
    

    searchusers= [];

    

  ionViewDidLoad() {

    this.Religion();
    this.Mothertongue();

  }

  Religion(){
    this.getData.masterselect('religion').subscribe(data =>{
      this.masterR = data.table      
    });
  }
  Mothertongue(){
    this.getData.masterselect('mothertongue').subscribe(data =>{
      this.masterM = data.table     
    });
  }


  Toast(Message : string){
    let toast = this.toastCtrl.create({
      message: Message,
      duration: 2000,
      position: 'bottom'
    });
    toast.present(toast);
  }


  search(){
    if(this.userSearch.gender != undefined && this.userSearch.mTongue != undefined && this.userSearch.religion != undefined && this.userSearch.fromAge != undefined && this.userSearch.toAge != undefined){

    // if(this.userData.password == this.repPassword){  
    this.authService.search(this.userSearch,'search').then((result) => {
      if(result['status'] == 'false'){
        this.messages  = result['message'];
        this.Toast('No users were found');
      }
      else{
        console.log(result,'--result--');
        console.log(this.userSearch.religion);
        localStorage.setItem('userSearch', JSON.stringify(this.responseData));
        this.navCtrl.push(HomePage);
      }
      // this.navCtrl.push(LoginPage);
      
    }, (err) => {
      console.log("Error",err);
      this.Toast('No users were found');
    });  
    //this.navCtrl.push(LoginPage)
  }else{
    this.Toast('Please Fill All Fileds.');
  }

}

}
