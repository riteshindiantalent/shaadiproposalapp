import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GetdataProvider } from '../../providers/getdata/getdata';
import { AuthService  } from '../../providers/getdata/authService';
import { ToastController } from 'ionic-angular';


@Component({
selector: 'page-step1',
templateUrl: 'step1.html',
})
export class Step1Page {

masterState : any[];
masterCity : any[];
masterCaste : any[];
masterR : any [];
masterCitizen : any[];
masterMstatus: any[];

state :any;
city :any;
Religion:any;
Caste:any;
residence:any;
marritalStatus:any;
pincode:any;
userid:any;
physicalStatus :any;

messages : string = '';
responseData : any;

data: any = null;
stateId:number;
religionId:number;

constructor(public navCtrl: NavController, public navParams: NavParams,private getData:GetdataProvider, private authService:AuthService,public toastCtrl: ToastController) {
this.masterCitizen= ["Citizen","Permanent Resident","Student Visa","Temporary Visa","Work permit"];
this.masterMstatus= ["Unmarried","Widow/Widower","Divorcee","Awating Divorce"]; 
this.state = navParams.get('state');
this.city = navParams.get('city');
this.Religion = navParams.get('Religion');
this.Caste = navParams.get('Caste');
this.residence = navParams.get('residence');
this.marritalStatus = navParams.get('marritalStatus');
this.pincode = navParams.get('pincode');

this.data = JSON.parse(localStorage.getItem('LoginuserData'));
     this.userid = this.data['user'][0]['userId'];
}

step1 = {"state":this.state,"city":this.city,"Religion":this.Religion,"Caste":this.Caste,"residence":this.residence,"marritalStatus":this.marritalStatus,"pincode":this.pincode,"userId":"17","physicalStatus":this.physicalStatus};

selectedDay: string = '';
selectChangeHandler ($event) {
this.stateId = $event;
console.log(this.stateId);
this.City(this.stateId);
}

selectChangeHandler2 ($event) {
    this.religionId = $event;
    console.log(this.religionId);
    this.caste(this.religionId);
}

ionViewDidLoad() {
this.State();
this.religion();
}

Toast(Message : string){
    let toast = this.toastCtrl.create({
      message: Message,
      duration: 2000,
      position: 'bottom'
    });
    toast.present(toast);
  }


religion(){
this.getData.masterselect('religion').subscribe(data =>{
this.masterR = data.table 
});
}
State(){
this.getData.masterselect('state').subscribe(data =>{
this.masterState = data.table 
console.log(this.masterState); 
});
}s

City(stateID){
this.getData.masterCity(stateID).subscribe(data =>{
this.masterCity = data.city 
});
}

caste(religionID){
    this.getData.mastercaste(religionID).subscribe(data =>{
    this.masterCaste = data.caste 
    });
}

Step1(){
    console.log(this.step1);
    if(this.step1.state != undefined && this.step1.city != undefined && this.step1.Religion != undefined
    && this.step1.Caste != undefined && this.step1.residence != undefined && this.step1.marritalStatus != undefined && this.step1.pincode != undefined && this.step1.physicalStatus != undefined){ 
    this.authService.Step1(this.step1).then((result) => {
      if(result['status'] == 'true'){
        this.responseData = result;
        localStorage.setItem('step1', JSON.stringify(this.responseData));
        this.messages  = result['message'];
        this.Toast(this.messages);
      }else{
        this.messages  = result['message'];
        this.Toast(this.messages);
      }
    }, (err) => {
      this.Toast('Somthing Wrong Please Try Again.');
      console.log("Error",err);
    });
    }else{
      this.Toast('Please Fill All Fileds.');
    }
  }

}