import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KeywordsearchPage } from './keywordsearch';

@NgModule({
  declarations: [
    KeywordsearchPage,
  ],
  imports: [
    IonicPageModule.forChild(KeywordsearchPage),
  ],
})
export class KeywordsearchPageModule {}
