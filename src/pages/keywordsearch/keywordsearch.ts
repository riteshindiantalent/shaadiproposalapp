import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { AuthService  } from '../../providers/getdata/authService';
import { ToastController } from 'ionic-angular';

/**
 * Generated class for the KeywordsearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-keywordsearch',
  templateUrl: 'keywordsearch.html',
})
export class KeywordsearchPage {

  data: any = [];
  firstName : string;
  lastName : string;
  userid: string;

  key: any = [];
  kfirstName : string;
  klastName : string;
  dob : string;
  hobby: string;
  interest: string;


  responseData : any;
  messages : string = '';
  userName :any;
  userId :any;

  constructor(public navCtrl: NavController,public authService : AuthService, public navParams: NavParams, public toastCtrl: ToastController) {
    this.userName= navParams.get('userName');
  }

  Keywordsearch = {"userName": this.userName,"userId": this.userId};

  ionViewDidLoad() {
    this.data = JSON.parse(localStorage.getItem('LoginuserData'));     
     this.firstName = this.data['user'][0]['firstName'];
     this.lastName = this.data['user'][0]['lastName'];
     this.userid = this.data['user'][0]['userId'];

     this.key = JSON.parse(localStorage.getItem('LoginuserData'));     
     this.kfirstName = this.key['user'][0]['firstName'];
     this.klastName = this.key['user'][0]['lastName'];
     this.userid = this.key['user'][0]['userId']
  }

  Toast(Message : string){
    let toast = this.toastCtrl.create({
      message: Message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present(toast);
  }

  keyword(){
    console.log(this.Keywordsearch);
     this.authService.keywordsearch(this.Keywordsearch,'keyword').then((result) => {
      console.log(result)
      if(result['status'] == 'true'){
        this.responseData = result;
        localStorage.setItem('Keywordsearch', JSON.stringify(this.responseData));
        console.log(result);
        this.messages  = result['message'];
        this.Toast("SignIn Successfully");
      }else{
        this.messages  = result['message'];
        this.Toast(this.messages);
      }
    }, (err) => {
      this.Toast('Somthing Wrong Please Try Again.');
      console.log("Error",err);
    });
  }

}
