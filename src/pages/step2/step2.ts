import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GetdataProvider } from '../../providers/getdata/getdata';

@Component({
  selector: 'page-step2',
  templateUrl: 'step2.html',
})
export class Step2Page {

  masterD : any [];
  masterO : any [];
  masterS : any [];
  masterEducation : any [];
  masterEmployee : any [];

  stateId:number;

  constructor(public navCtrl: NavController, public navParams: NavParams,private getData:GetdataProvider) {
    this.masterEducation= ["Doctors","Masters","Degrees","Bachelors","Associates Degree","Diploma","High School","Less than high school","Trade school","Undergraduate"];
    this.masterEmployee= ["Private","Government","Business","Defence","No Employed","Other"];
  }

  ionViewDidLoad() {
    this.Degree();
    this.Occupation();
    this.Salary();
  }
  Degree(){
    this.getData.masterselect('degree').subscribe(data =>{
    this.masterD = data.table 
    console.log(this.masterD);
    });
    }
  Occupation(){
    this.getData.masterselect('occupation').subscribe(data =>{
    this.masterO = data.table 
    console.log(this.masterO);
    });
  }
  Salary(){
    this.getData.masterselect('salarymaster').subscribe(data =>{
    this.masterS = data.table 
    console.log(this.masterS);
    });
  }

}
