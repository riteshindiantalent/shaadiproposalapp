import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PincodesearchPage } from './pincodesearch';

@NgModule({
  declarations: [
    PincodesearchPage,
  ],
  imports: [
    IonicPageModule.forChild(PincodesearchPage),
  ],
})
export class PincodesearchPageModule {}
