import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthService  } from '../../providers/getdata/authService';
import { ToastController } from 'ionic-angular';

/**
 * Generated class for the PincodesearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pincodesearch',
  templateUrl: 'pincodesearch.html',
})
export class PincodesearchPage {

  data: any = [];
  firstName : string;
  lastName : string;
  userid: string;

  key: any = [];
  kfirstName : string;
  klastName : string;
  dob : string;
  hobby: string;
  interest: string;


  responseData : any;
  messages : string = '';
  SPID :any;
  userId :any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public authService : AuthService, public toastCtrl: ToastController) {
    this.data = JSON.parse(localStorage.getItem('LoginuserData'));     
     this.firstName = this.data['user'][0]['firstName'];
     this.lastName = this.data['user'][0]['lastName'];
     this.userid = this.data['user'][0]['userId'];
     console.log(this.data);
  }

  Pincodesearch = {"SPID": this.SPID,"userId": this.userId};

  ionViewDidLoad() {
    console.log('ionViewDidLoad PincodesearchPage');
  }

  Toast(Message : string){
    let toast = this.toastCtrl.create({
      message: Message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present(toast);
  }

  pincodesearch(){
    console.log(this.Pincodesearch);
     this.authService.pincodesearch(this.Pincodesearch,'keyword').then((result) => {
      console.log(result)
      if(result['status'] == 'true'){
        this.responseData = result;
        localStorage.setItem('Pincodesearch', JSON.stringify(this.responseData));
        console.log(result);
        this.messages  = result['message'];
        this.Toast(this.messages);
      }else{
        this.messages  = result['message'];
        this.Toast(this.messages);
      }
    }, (err) => {
      this.Toast('Somthing Wrong Please Try Again.');
      console.log("Error",err);
    });
  }

}
