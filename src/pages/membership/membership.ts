import { Component, ViewChild, Renderer } from '@angular/core';
import { IonicPage, NavController, NavParams, CardContent } from 'ionic-angular';

/**
 * Generated class for the MembershipPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-membership',
  templateUrl: 'membership.html',
})
export class MembershipPage {

  package = [
    {
      price:"100",
      title:"SILVER",
      feature1:"Email Support",
      feature2:"10 (Match Profiles)",
      feature3:"Basic Search",
      feature4:"Advance Search",
      feature5:"Aadhar Card Verification",
      feature6:"Photo Verification",
    },
    {
      price:"200",
      title:"GOLD",
      feature1:"Email Support",
      feature2:"10 (Match Profiles)",
      feature3:"Basic Search",
      feature4:"Advance Search",
      feature5:"Aadhar Card Verification",
      feature6:"Photo Verification",
    },
    {
      price:"300",
      title:"PLATINUM",
      feature1:"Email Support",
      feature2:"10 (Match Profiles)",
      feature3:"Basic Search",
      feature4:"Advance Search",
      feature5:"Aadhar Card Verification",
      feature6:"Photo Verification",
    }
  ]
  

  expand = false;
   @ViewChild("cc1") cardContent:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public renderer:Renderer) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MembershipPage');
  }

  toggleAccordion(){
    if(this.expand){
      this.renderer.setElementStyle(this.cardContent.nativeElement, "max-height", "0px");
    }else{
      this.renderer.setElementStyle(this.cardContent.nativeElement, "max-height", "400px");
    }
  }
}
