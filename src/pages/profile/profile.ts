import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { AuthService  } from '../../providers/getdata/authService';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  public user:any;
  data: any = [];
  firstName : string;
  lastName : string;
  userid: string;
  email: any;
  dob: string;
  mobile : string;
  hobby : string;
  interest: string;
  profile : string;

  basicinfo = [
    {
      CreatedFor:"Self",
      ProfileText:"Marty McFly",
      Gender:"Male",
      DateofBirth:"24-07-1996",
      Hobby:"",
      Interest:"",
      EducationDegree:"Degrees In Administrative Services",
      AnnualIncome:"Less Than 1 Lakh",
      Occupation:"Company Secretary",
      Employedin:"Private",
      Mobile:"8692972118",
      Phone:"8692972118",
      Address:"N/A",
      Residence:"Citizen",
      Country:"India",
      State:"Assam",
      City:"Guwahati",
      Pincode:"4000078",
    }
  ]

  profileview:string = "info";
  isAndroid:boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams,public authService : AuthService, platform: Platform) {
    this.isAndroid = platform.is("Android")

    
  }

  ionViewDidLoad() {
    this.data = JSON.parse(localStorage.getItem('LoginuserData'));     
     this.firstName = this.data['user'][0]['firstName'];
     this.lastName = this.data['user'][0]['lastName'];
     this.dob = this.data['user'][0]['dob'];
     this.email = this.data['user'][0]['email'];
     this.mobile = this.data['user'][0]['mobile'];
     this.hobby = this.data['user'][0]['hobby'];
     this.interest = this.data['user'][0]['interest'];
     this.profile = this.data['user'][0]['profile'];
  }

}
