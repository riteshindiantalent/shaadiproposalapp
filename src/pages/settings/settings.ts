import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthService  } from '../../providers/getdata/authService';
import { ToastController } from 'ionic-angular';


/**
 * Generated class for the SettingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  oldPassword :any;
  newPassword :any;
  repnewpassword : any;
  responseData : any;
  messages : string = '';
  data: any = [];
  userid: string;
  userId :any;

  constructor(public navCtrl: NavController, public navParams: NavParams,private authService:AuthService,public toastCtrl: ToastController) {
    this.oldPassword = navParams.get('oldPassword');
    this.newPassword = navParams.get('newPassword');
    this.repnewpassword = navParams.get('repnewpassword');

    this.data = JSON.parse(localStorage.getItem('LoginuserData'));     
    this.userid = this.data['user'][0]['userId'];
  }

  changepassword = {"oldPassword": this.oldPassword,"newPassword":this.newPassword,"userId":this.userId}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingPage');
    
  }

  Toast(Message : string){
    let toast = this.toastCtrl.create({
      message: Message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present(toast);
  }

  passwordchange(){
    console.log(this.changepassword);
    if(this.changepassword.oldPassword != undefined && this.changepassword.newPassword != undefined)
    {
     this.authService.passwordchange(this.changepassword).then((result) => {
      if(result['status'] == 'true')
      {
        console.log("hellow");
        console.log(result);
        this.responseData = result;
        localStorage.setItem('changepassword', JSON.stringify(this.responseData));
        this.messages  = result['message'];
        this.Toast(this.messages);
      }else
      {
        this.messages  = result['message'];
        this.Toast(this.messages);
      }
    }, (err) => {
      this.Toast('Somthing Wrong Please Try Again.');
      console.log("Error",err);
    });
    }else
    {
      this.Toast('Please Fill All Fileds.');
    }
  }

  

}
