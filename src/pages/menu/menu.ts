import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, App} from 'ionic-angular';
import { Nav,Platform } from 'ionic-angular';
import { SMS } from '@ionic-native/sms';
import { CallNumber } from '@ionic-native/call-number';

import { HomePage } from '../home/home';
import { LoginPage } from '../login/login';
import { AboutPage } from '../about/about';
import { SearchPage } from '../search/search';
import { SettingsPage } from '../settings/settings';
import { MembershipPage } from '../membership/membership';
import { ProfilePage } from '../profile/profile';
import { SignupPage } from '../signup/signup';
import { AdvancesearchPage } from '../advancesearch/advancesearch';
import { LocationsearchPage } from '../locationsearch/locationsearch';
import { PincodesearchPage } from '../pincodesearch/pincodesearch';
import { KeywordsearchPage } from '../keywordsearch/keywordsearch';
import { ProfilesearchPage } from '../profilesearch/profilesearch';

import { AuthService  } from '../../providers/getdata/authService';

/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  public userDetails:any;
 
  data: any = [];
  firstName : string;
  lastName : string;
  products:string;
  userlogin:boolean = false;

  @ViewChild(Nav) nav: Nav;
  rootPage:any = ProfilePage;

  pages: Array<{title: string, component: any, logo:string}>;
  activePage: any;

  constructor(public platform: Platform, public navCtrl: NavController, public navParams: NavParams, private sms: SMS, public app:App, public authService : AuthService, private callNumber: CallNumber) {
    
    //  this.data = JSON.parse(localStorage.getItem('LoginuserData'));     
    //  this.firstName = this.data['user'][0]['firstName'];
    //  this.lastName = this.data['user'][0]['lastName'];

    this.getusername();
   
     this.initializeApp();    
     this.pages = [
      { title: 'Home', component: HomePage, logo:'md-home'},
      { title: 'About us', component: AboutPage, logo:'md-alert'},
      { title: 'Search', component: SearchPage, logo:'md-search'},
      { title: 'Settings', component: SettingsPage, logo:'md-settings'},
      { title: 'Membership', component: MembershipPage, logo:'md-contacts'},
      { title: 'Profile', component: ProfilePage, logo:'md-contact'},
      { title: 'Advancesearch', component: AdvancesearchPage, logo:'md-contact'},
      { title: 'Locationsearch', component: LocationsearchPage, logo:'md-pin'},
      { title: 'Pincodesearch', component: PincodesearchPage, logo:'md-contact'},
      { title: 'Keywordsearch', component: KeywordsearchPage, logo:'md-contact'},
      { title: 'Profilesearch', component: ProfilesearchPage, logo:'md-contact'}
      ];
      this.activePage = this.pages[0];

  }

  initializeApp(): any {
    this.platform.ready().then(()=>{
      // StatusBar.styleDefault();
      // SplashScreen.hide();
    });
  }

  openPage(page){
    this.nav.setRoot(page.component)
    this.activePage = page;
  }

  checkActive(page){
    return page == this.activePage;
  }

  login(){
    this.navCtrl.push(LoginPage)
  }

  signup(){
    this.navCtrl.push(SignupPage)
  }

  async call():Promise<any>{
    try{
      await this.callNumber.callNumber('18002669192', true);
    }catch(e){
      console.error(e);
    }
    
  //   this.callNumber.callNumber('18002669192', true)
  // .then(res => console.log('Launched dialer!', res))
  // .catch(err => console.log('Error launching dialer', err));
  }

  sendsms(){
    var options: {
      replaceLineBreaks:true,
      android:{
        intent: 'INTENT'
      }
    }
    this.sms.send("8692972118",'Message', options)
  .then(res => console.log('Launched dialer!', res))
  .catch(err => console.log('Error launching dialer', err));
  }

  getusername(){
    this.data = JSON.parse(localStorage.getItem('LoginuserData'));     
     this.firstName = this.data['user'][0]['firstName'];
     this.lastName = this.data['user'][0]['lastName'];
     this.userlogin = true;
  }
}
