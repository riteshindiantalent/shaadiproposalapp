import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GetdataProvider } from '../../providers/getdata/getdata';

/**
 * Generated class for the Step3Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-step3',
  templateUrl: 'step3.html',
})
export class Step3Page {

  masterS : any [];
  masterM : any[];
  masterMstatus: any[];
  timeStarts: '07:43';

  constructor(public navCtrl: NavController, public navParams: NavParams,private getData:GetdataProvider) {
  }

  ionViewDidLoad() {
    this.Star();
    this.Moon();
  }
  Star(){
    this.getData.masterselect('star').subscribe(data =>{
    this.masterS = data.table 
    });
  }
  Moon(){
    this.getData.masterselect('moonsign').subscribe(data =>{
    this.masterM = data.table 
    });
  }

}
