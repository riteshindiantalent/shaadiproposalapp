import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GetdataProvider } from '../../providers/getdata/getdata';

/**
 * Generated class for the AdvancesearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-advancesearch',
  templateUrl: 'advancesearch.html',
})
export class AdvancesearchPage {

  religion :any;
  userId:any;
  caste:any;
  gothra :any;
  star:any;
  moonsign:any;
  manglik:any;

  masterR: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams, private getData:GetdataProvider) {
    this.religion = navParams.get('religion');
  }

  advSearch = {"userId":this.userId,"religion":this.religion,"caste":this.caste,"gothra":this.gothra,"star":this.star,"moonsign":this.moonsign,"manglik":this.manglik};

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdvancesearchPage');
  }

  Religion(){
    this.getData.masterselect('religion').subscribe(data =>{
      this.masterR = data.table      
    });
  }

}
