import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdvancesearchPage } from './advancesearch';

@NgModule({
  declarations: [
    AdvancesearchPage,
  ],
  imports: [
    IonicPageModule.forChild(AdvancesearchPage),
  ],
})
export class AdvancesearchPageModule {}
