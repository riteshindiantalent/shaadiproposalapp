import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup } from '@angular/forms';
import { ToastController } from 'ionic-angular';

import { SignupPage } from '../signup/signup';
import { ForgotpasswordPage } from '../forgotpassword/forgotpassword';
import { SearchPage } from '../search/search';
import { AuthService  } from '../../providers/getdata/authService';
import { MenuPage } from '../menu/menu';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {

  loginuser = false;

  splash = true;
  formgroup: FormGroup;
  loginWith :any;
  password :any;
  responseData : any;
  messages : string = '';

  constructor(public navCtrl: NavController,private authService:AuthService, public toastCtrl: ToastController,public navParams: NavParams) {
    
    this.loginWith= navParams.get('loginWith');
    this.password = navParams.get('password');
    
  }

  LoginuserData = {"loginWith": this.loginWith,"password": this.password,"userFrom": "APP"};

  ionViewDidLoad() {
    setTimeout(()=>this.splash = false, 3000,);
  }

  Toast(Message : string){
    let toast = this.toastCtrl.create({
      message: Message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present(toast);
  }

  login(){
    if(this.LoginuserData.loginWith != undefined && this.LoginuserData.password != undefined)
    {
     this.authService.Login(this.LoginuserData,'login').then((result) => {
      if(result['status'] == 'true')
      {
        this.responseData = result;
        localStorage.setItem('LoginuserData', JSON.stringify(this.responseData));
        this.messages  = result['message'];
        this.Toast("SignIn Successfully");
        this.navCtrl.push(MenuPage);
      }else
      {
        this.messages  = result['message'];
        this.Toast(this.messages);
      }
    }, (err) => {
      this.Toast('Somthing Wrong Please Try Again.');
      console.log("Error",err);
    });
    }else
    {
      this.Toast('Please Fill All Fileds.');
    }
  }

  signup(){
    this.navCtrl.push(SignupPage)
  }

  forpassword(){
    this.navCtrl.push(ForgotpasswordPage)
  }

  search(){
    this.navCtrl.push(SearchPage)
  }

  check(){
    this.loginuser = true;
  }

}
