import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthService  } from '../../providers/getdata/authService';
import { ToastController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-forgotpassword',
  templateUrl: 'forgotpassword.html',
})
export class ForgotpasswordPage {

  responseData : any;
  messages : string = '';
  forgotWith : any;

  constructor(public navCtrl: NavController, private authService:AuthService, public navParams: NavParams, public toastCtrl: ToastController) {
  }

  forgot = {"forgotWith": this.forgotWith};

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotpasswordPage');
  }

  Toast(Message : string){
    let toast = this.toastCtrl.create({
      message: Message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present(toast);
  }

  forgotpassword(){
    if(this.forgot.forgotWith != undefined)
    {
     this.authService.forgotpassword(this.forgot).then((result) => {
      if(result['status'] == 'true')
      {
        this.responseData = result;
        localStorage.setItem('forgot', JSON.stringify(this.responseData));
        this.messages  = result['message'];
        this.Toast("Please check your mail..");
      }else
      {
        this.messages  = result['message'];
        this.Toast(this.messages);
      }
    }, (err) => {
      this.Toast('Somthing Wrong Please Try Again.');
      console.log("Error",err);
    });
    }else
    {
      this.Toast('Please enter your email.');
    }
  }

}
