import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GetdataProvider } from '../../providers/getdata/getdata';
import { AuthService  } from '../../providers/getdata/authService';
import { ToastController } from 'ionic-angular';

/**
 * Generated class for the LocationsearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-locationsearch',
  templateUrl: 'locationsearch.html',
})
export class LocationsearchPage {

  data: any = [];
  userid: string;
  state:any;
  city:any;

  responseData : any;
  messages : string = '';
  SPID :any;
  userId :any;

  masterState:any [];
  masterCity:any[];

  constructor(public navCtrl: NavController, public navParams: NavParams,public getdata:GetdataProvider, public authService : AuthService,public toastCtrl: ToastController) {
    this.data = JSON.parse(localStorage.getItem('LoginuserData'));
     this.userid = this.data['user'][0]['userId'];
  }

  // locationsearch = {"state":this.state, "city":this.city, "userid":this.userid}

  ionViewDidLoad() {
    this.State();
  }

  State()
  {
    this.getdata.masterselect('state').subscribe(data=>
      {
        this.masterState= data.table
      });
  }

  // locationsearch(){
  //   console.log(this.Pincodesearch);
  //    this.authService.pincodesearch(this.locationsearch,'keyword').then((result) => {
  //     console.log(result)
  //     if(result['status'] == 'true'){
  //       this.responseData = result;
  //       localStorage.setItem('locationsearch', JSON.stringify(this.responseData));
  //       console.log(result);
  //       this.messages  = result['message'];
  //       this.Toast(this.messages);
  //     }else{
  //       this.messages  = result['message'];
  //       this.Toast(this.messages);
  //     }
  //   }, (err) => {
  //     this.Toast('Somthing Wrong Please Try Again.');
  //     console.log("Error",err);
  //   });
  // }

  
  

}
