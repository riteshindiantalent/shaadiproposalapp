import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
// import { HomePage } from '../home/home';
// import { FormBuilder, FormGroup, AbstractControl, Validators} from '@angular/forms';
// import { AlertController } from 'ionic-angular';
// import { MenuPage } from '../menu/menu';
import { LoginPage } from '../login/login';

import { AuthService  } from '../../providers/getdata/authService';
/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  firstName :any;
  lastName :any;
  email :any;
  mobile :any;
  birthDate :any;
  mTongue :any;
  gender :any = "male";
  createdFor :any = "Self";
  userFrom :any;
  password :any;
  repPassword : any;
  messages : string = '';
  responseData : any;

  mothertongue: string = "select";

  
  public event = {
    birthDate: '1990-02-19',
  } 
  radioChecked($value){
   this.gender = $value;
 }

  constructor(public navCtrl: NavController,private authService:AuthService,public toastCtrl: ToastController,public navParams: NavParams) {
    this.firstName = navParams.get('firstName');
    this.lastName = navParams.get('lastName');
    this.email= navParams.get('email');
    this.mobile = navParams.get('mobile');
    this.birthDate= navParams.get('birthDate');
    this.mTongue = navParams.get('mTongue');
    this.gender = navParams.get('gender');
    this.createdFor = navParams.get('createdFor');
    this.userFrom = navParams.get('userFrom');
    this.password = navParams.get('password');
    this.repPassword = navParams.get('repPassword');
  }

  userData = {"firstName": this.firstName,"lastName":this.lastName, "email": this.email,"mobile": this.mobile,"password": this.password,"birthDate": this.birthDate,"mTongue": "Marthi","gender": this.gender,"createdFor": this.createdFor,"userFrom": "Android"};

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }


  Toast(Message : string){
    let toast = this.toastCtrl.create({
      message: Message,
      duration: 2000,
      position: 'bottom'
    });
    toast.present(toast);
  }


  register(){
    console.log(this.password);
   
    console.log(this.userData);
    if(this.userData.firstName != undefined && this.userData.lastName != undefined && this.userData.email != undefined && this.userData.password != undefined){

    if(this.userData.password == this.repPassword){  
    this.authService.postData(this.userData,'register').then((result) => {

      if(result['status'] == 'true'){
        this.responseData = result;
        localStorage.setItem('userData', JSON.stringify(this.responseData));
        this.messages  = result['message'];
        this.Toast("Successfully Registered");
       this.navCtrl.push(LoginPage);
      }else{
        this.messages  = result['message'];
        this.Toast(this.messages);
      }
     
  
     
      // this.navCtrl.push(LoginPage);
      
    }, (err) => {
      this.Toast('Somthing Wrong Please Try Again.');
      console.log("Error",err);
    });
    }else{
      this.Toast('Password Does Not Match');
    }
    }else{
     
      this.Toast('Please Fill All Fileds.');
     
    }


    
    //this.navCtrl.push(LoginPage)
  }

}
