import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GetdataProvider } from '../../providers/getdata/getdata';

/**
 * Generated class for the Step4Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-step4',
  templateUrl: 'step4.html',
})
export class Step4Page {

  masterHeight : any [];
  masterBlood : any [];
  masterComplexion : any [];
  masterweight : any [];
  masterDiet : any [];

  constructor(public navCtrl: NavController, public navParams: NavParams,private getData:GetdataProvider) {
    this.masterComplexion = ["Does not matter","Very Fair","Fair","wheatish","Wheatish Medium","Wheatish Brown","Dark"];
    this.masterDiet = ["Veg","Eggetarian","Occassionally Non-Veg","Non-Veg","Jain","Vegan"];
  }

  ionViewDidLoad() {
    this.Height();
    this.Blood();
  }

  Height(){
    this.getData.masterselect('heightmaster').subscribe(data =>{
    this.masterHeight = data.table 
    });
  }
  Blood(){
    this.getData.masterselect('bloodgroup').subscribe(data =>{
    this.masterBlood = data.table 
    });
  }

}
