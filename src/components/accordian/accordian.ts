import { Component, ViewChild, Renderer } from '@angular/core';

/**
 * Generated class for the AccordianComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'accordian',
  templateUrl: 'accordian.html'
})
export class AccordianComponent {

  package = [
    {
      price:"100",
      title:"SILVER",
      feature1:"Email Support",
      feature2:"10 (Match Profiles)",
      feature3:"Basic Search",
      feature4:"Advance Search",
      feature5:"Aadhar Card Verification",
      feature6:"Photo Verification",
    },
    {
      price:"200",
      title:"GOLD",
      feature1:"Email Support",
      feature2:"10 (Match Profiles)",
      feature3:"Basic Search",
      feature4:"Advance Search",
      feature5:"Aadhar Card Verification",
      feature6:"Photo Verification",
    },
    {
      price:"300",
      title:"PLATINUM",
      feature1:"Email Support",
      feature2:"10 (Match Profiles)",
      feature3:"Basic Search",
      feature4:"Advance Search",
      feature5:"Aadhar Card Verification",
      feature6:"Photo Verification",
    }
  ]

  
  expand = false;
   @ViewChild("cc1") cardContent:any;

  constructor(public renderer:Renderer) {
  }

  toggleAccordion(cc1){
    if(this.expand){
      this.renderer.setElementStyle(this.cardContent.nativeElement, "max-height", "0px");
    }else{
      this.renderer.setElementStyle(this.cardContent.nativeElement, "max-height", "400px");
    }
  }

}
