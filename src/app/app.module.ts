import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { Network } from '@ionic-native/network';
import { FormsModule } from '@angular/forms';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AboutPage } from '../pages/about/about';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { ForgotpasswordPage } from '../pages/forgotpassword/forgotpassword';
import { MenuPage } from '../pages/menu/menu';
import { SearchPage } from '../pages/search/search';
import { SettingsPage } from '../pages/settings/settings';
import { UserprofilePage } from '../pages/userprofile/userprofile';
import { MembershipPage } from '../pages/membership/membership';

import { AccordianComponent } from '../components/accordian/accordian';
import { ProfilePage } from '../pages/profile/profile';
import { GetdataProvider } from '../providers/getdata/getdata';
import { NetworkProvider } from '../providers/network/network';
import { SMS } from '@ionic-native/sms';
import { CallNumber } from '@ionic-native/call-number';

import { AuthService  } from '../providers/getdata/authService';
import { AdvancesearchPage } from '../pages/advancesearch/advancesearch';
import { LocationsearchPage } from '../pages/locationsearch/locationsearch';
import { PincodesearchPage } from '../pages/pincodesearch/pincodesearch';
import { KeywordsearchPage } from '../pages/keywordsearch/keywordsearch';
import { ProfilesearchPage } from '../pages/profilesearch/profilesearch';
import { Step1Page } from '../pages/step1/step1';
import { Step2Page } from '../pages/step2/step2';
import { Step3Page } from '../pages/step3/step3';
import { Step4Page } from '../pages/step4/step4';

@NgModule({
  declarations: [
    MyApp, 
    HomePage,
    AboutPage,
    LoginPage,
    SignupPage,
    MenuPage,
    ForgotpasswordPage,
    SearchPage,
    SettingsPage,
    UserprofilePage,
    MembershipPage,
    ProfilePage,
    AccordianComponent,
    AdvancesearchPage,
    LocationsearchPage,
    PincodesearchPage,
    KeywordsearchPage,
    ProfilesearchPage,
    Step1Page,
    Step2Page,
    Step3Page,
    Step4Page
    
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AboutPage,
    LoginPage,
    SignupPage,
    MenuPage,
    ForgotpasswordPage,
    SearchPage,
    SettingsPage,
    UserprofilePage,
    ProfilePage,
    MembershipPage,
    AdvancesearchPage,
    LocationsearchPage,
    PincodesearchPage,
    KeywordsearchPage,
    ProfilesearchPage,
    Step1Page,
    Step2Page,
    Step3Page,
    Step4Page
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Network,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GetdataProvider,
    NetworkProvider,
    AuthService,
    SMS,
    CallNumber
  ]
})
export class AppModule {}
