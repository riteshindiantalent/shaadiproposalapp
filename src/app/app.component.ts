import { Component } from '@angular/core';
import { Platform, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network';
import { LoginPage } from '../pages/login/login';
import { Step1Page } from '../pages/step1/step1';
import { Step2Page } from '../pages/step2/step2';
import { NetworkProvider } from '../providers/network/network';
import { Step3Page } from '../pages/step3/step3';
import { Step4Page } from '../pages/step4/step4';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = Step1Page;

  constructor(public platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,public events: Events,
    public network: Network,
    public networkProvider: NetworkProvider) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  initializeApp() {

    this.platform.ready().then(() => {

      this.networkProvider.initializeNetworkEvents();

     // Offline event
  this.events.subscribe('network:offline', () => {
      alert('network:offline ==> '+this.network.type);    
  });

  // Online event
  this.events.subscribe('network:online', () => {
      alert('network:online ==> '+this.network.type);        
  });

    });
}
}

