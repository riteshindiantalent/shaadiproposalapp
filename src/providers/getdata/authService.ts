import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/ADD/operator/map';

// ---------------------Local Apis
// let apiUrl = 'http://192.168.0.188/API/';
// let login = "http://192.168.0.188/API/login";
// let bsearch = "http://192.168.0.188/API/search";
// let ksearch = "http://192.168.0.188/API/keywordsearch";
// let fpassword = "http://192.168.0.188/API/forgotpassword";
// let advsearch = "http://192.168.0.188/API/advancesearch";
// let locsearch = "http://192.168.0.188/API/locationsearch";
// let profsearch = "http://192.168.0.188/API/profilesearch";
// let pinsearch = "http://192.168.0.188/API/pincodesearch";
// let step1 = "http://192.168.0.188/API/step1";
// let step2 = "http://192.168.0.188/API/step2";
// let step3 = "http://192.168.0.188/API/step3";
// let step4 = "http://192.168.0.188/API/step4";

// ---------------------Online Apis
let apiUrl = 'http://api.shaadiproposal.com/';
let login = "http://api.shaadiproposal.com/login";
let bsearch = "http://api.shaadiproposal.com/search";
let ksearch = "http://api.shaadiproposal.com/keywordsearch";
let fpassword = "http://api.shaadiproposal.com/forgotpassword";
let advsearch = "http://api.shaadiproposal.com/advancesearch";
let locsearch = "http://api.shaadiproposal.com/locationsearch";
let profsearch = "http://api.shaadiproposal.com/profilesearch";
let pinsearch = "http://api.shaadiproposal.com/pincodesearch";
let changepass = "http://api.shaadiproposal.com/changepassword";
let step1 = "http://api.shaadiproposal.com/step1";
let step2 = "http://api.shaadiproposal.com/step2";
let step3 = "http://api.shaadiproposal.com/step3";
let step4 = "http://api.shaadiproposal.com/step4";

@Injectable()
export class AuthService {

  constructor(public http : Http) {
  }

  postData(credentials, type) {
  
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.post(apiUrl + type, JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          resolve(res.json());
        }, (err) => {
          console.log('ERROR',err);
          reject(err);
        });
    });

  }

  // function made for login
  Login(credentials, type) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      console.log(credentials);
      this.http.post(login, JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          resolve(res.json());
        }, (err) => {
          console.log('ERROR',err);
          reject(err);
        });
    });
  }
  
  // fungtion made for basic search
  search(credentials, type) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      console.log(credentials);
      this.http.post(bsearch, JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          
          resolve(res.json());
        }, (err) => {
          console.log('ERROR',err);
          reject(err);
        });
    });

  }

  // function made for keyword search
  keywordsearch(credentials, type) {
  
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      console.log(credentials);
      this.http.post(ksearch, JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          
          resolve(res.json());
        }, (err) => {
          console.log('ERROR',err);
          reject(err);
        });
    });

  }

  // function made for keyword search
  pincodesearch(credentials, type) {
  
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      console.log(credentials);
      this.http.post(pinsearch, JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          
          resolve(res.json());
        }, (err) => {
          console.log('ERROR',err);
          reject(err);
        });
    });

  }

  // function made for keyword search
  profilesearch(credentials, type) {
  
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      console.log(credentials);
      this.http.post(profsearch, JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          
          resolve(res.json());
        }, (err) => {
          console.log('ERROR',err);
          reject(err);
        });
    });

  }

  locationsearch(credentials, type) {
  
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      console.log(credentials);
      this.http.post(locsearch, JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          
          resolve(res.json());
        }, (err) => {
          console.log('ERROR',err);
          reject(err);
        });
    });

  }

  // function made for Forgotpassword
  forgotpassword(credentials) {
  
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      console.log(credentials);
      this.http.post(fpassword, JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          
          resolve(res.json());
        }, (err) => {
          console.log('ERROR',err);
          reject(err);
        });
    });

  }

  passwordchange(credentials) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.post(changepass, JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          resolve(res.json());
        }, (err) => {
          console.log('ERROR',err);
          reject(err);
        });
    });

  }

  Step1(credentials) {
  
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      console.log(credentials);
      this.http.post(step1, JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          
          resolve(res.json());
        }, (err) => {
          console.log('ERROR',err);
          reject(err);
        });
    });

  }

}