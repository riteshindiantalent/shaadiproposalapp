import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/do';

@Injectable()
export class GetdataProvider {

  private headers:Headers;

  //private url:string = "http://192.168.0.108:8080/users"; //Nodejs api

  // ---------------Local Apis
  // private url:string = "http://192.168.0.188/API/users";
  // private master:string = "http://192.168.0.188/API/master";
  // private city:string = "http://192.168.0.188/API/city";
  // private caste:string = "http://192.168.0.188/API/caste";

  // --------------Online Apis
  private url:string = "http://api.shaadiproposal.com/users";
  private master:string = "http://api.shaadiproposal.com/master";
  private masterC :string = "http://api.shaadiproposal.com/city";
  private caste:string = "http://api.shaadiproposal.com/caste";

  constructor(private http: Http) {
    this.headers=new Headers();
    this.headers.append('Content-Type', 'application/json; charset=UTF-8');
    this.headers.append('Accept','application/json');
  }

  getdata(){
    let a=new RequestOptions({headers:this.headers});
    return this.http.get(this.url,a)
    .map((res:Response) => res.json());
  }

  masterselect(table){
    let m = new RequestOptions({headers:this.headers});
    return this.http.get(this.master+"?table="+table,m)
    .map((res:Response)=>res.json());
  }

  masterCity(stateID){
    let m = new RequestOptions({headers:this.headers});
    return this.http.get(this.masterC+"?stateId="+stateID,m)
    .map((res:Response)=>res.json());
    }

    mastercaste(religionID){
      let m = new RequestOptions({headers:this.headers});
      return this.http.get(this.caste+"?religionId="+religionID,m)
      .map((res:Response)=>res.json());
    }

}
